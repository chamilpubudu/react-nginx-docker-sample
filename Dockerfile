FROM node AS builder

WORKDIR /app
COPY /app .

RUN npm install
RUN npm run build

FROM nginx:stable

COPY --from=builder /app/build/ /var/www

COPY ./nginx.conf /etc/nginx/conf.d/default.conf
